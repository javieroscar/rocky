<?php get_header(); ?>
<div class="container">
	<div class="main-and-sidebar">
		<main class="main">
			<?php if( have_posts() ): while( have_posts() ): 
				the_post(); ?>
				<article>
					<?php the_post_thumbnail(); ?>
					<h2 class="title"><?php the_title(); ?></h2>
					<?php the_content(); ?>
				</article>
			<?php endwhile; endif; ?>
		</main>
		<aside class="sidebar">
			<h4 class="sidebar_title">Otras Zapatillas</h4>
			<ul class="sidebar_list">
				<li><a href="#" class="sidebar_link">Nike Air Max</a></li>
				<li><a href="#" class="sidebar_link">Nike Running</a></li>
				<li><a href="#" class="sidebar_link">Nike Sport</a></li>
			</ul>
		</aside>
	</div>
</div>

<?php get_footer(); ?>