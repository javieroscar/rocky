<?php get_header(); ?>

<div class="container">
	<div class="main-and-sidebar">
		<main class="main">
			<?php if( have_posts() ) : while( have_posts() ): the_post() ?>
				<?php the_post_thumbnail(); ?>
				<h2 class="title"><?php the_title(); ?></h2>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>  
		</main>
		<?php //get_sidebar(); ?>
		<!-- <aside class="sidebar">
			<h4 class="sidebar_title">Otros Sitios</h4>
			<ul class="sidebar_list">
				<li><a href="#" class="sidebar_link">Reebok</a></li>
				<li><a href="#" class="sidebar_link">Puma</a></li>
				<li><a href="#" class="sidebar_link">Adidas</a></li>
			</ul>
			<h4 class="sidebar_title">Visitanos en:</h4>
			<ul class="socials">
				<li><a href="#" class="socials_link"><span class="icon-facebook"></span></a></li>
				<li><a href="#" class="socials_link"><span class="icon-youtube"></span></a></li>
				<li><a href="#" class="socials_link"><span class="icon-instagram"></span></a></li>
			</ul>
		</aside> -->
	</div>
</div>

<?php get_footer(); ?>