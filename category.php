<?php
	get_header();
?>
	<div class="container">
		<main class="main main--full">

				<section>
					<h2 class="title"><?php single_cat_title(); ?></h2>
						<?php if( have_posts() ): ?> 
						<ul class="souplist">
							<?php while ( have_posts() ): the_post(); ?>

									<li class="souplist_item">
										<article class="soup">
											<div class="soup_img">
												<?php the_post_thumbnail(); ?>
											</div>
											<div class="soup_text">
												<h2 class="soup_title"><?php  the_title(); ?></h2>
												<?php the_excerpt(); ?>
												<a href="<?php the_permalink(); ?>" class="soup_link">Ver receta</a>
											</div>
										</article>
									</li>

							<?php endwhile; ?>
						</ul>
						<?php the_posts_pagination( array( 'mid_size' => 2, 'prev_text' => 'Atrás', 'next_text' => 'Siguiente' ) ); ?>
						<?php endif; ?>
				</section>
		</main>
	</div>

<?php
	get_footer();
?>