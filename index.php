<?php
	get_header();
?>
	<div class="container">
		<main class="main main--full">

				<section>
					<h2 class="title">Nuestras Zapatillas</h2>
						<?php if( have_posts() ): ?> 
						<ul class="souplist">
							<?php while ( have_posts() ): the_post(); ?>
								<?php if(in_category('zapatillas') ): ?>
									<li class="souplist_item">
										<article class="soup">
											<div class="soup_img">
												<?php the_post_thumbnail(); ?>
											</div>
											<div class="soup_text">
												<h2 class="soup_title"><?php  the_title(); ?></h2>
												<?php the_excerpt(); ?>
												<a href="<?php the_permalink(); ?>" class="soup_link">Ver receta</a>
											</div>
										</article>
									</li>
								<?php endif; ?>
							<?php endwhile; ?>
						</ul>
						<?php endif; ?>
				</section>
		</main>
	</div>

<?php
	get_footer();
?>