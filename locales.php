<?php/*
Template Name: locales
*/
?>
<?php get_header(); ?>
<div class="container">
	<div class="locales-principal">
		<?php $queryposts1 = new WP_Query( array('category_name' => 'locales') ); if( $queryposts1->have_posts() ): ?>
					<div class="locales">
					<?php while( $queryposts1->have_posts() ): $queryposts1->the_post(); ?>
						<div class="localesdos">
							<h3><?php the_title(); ?></h3>
							<p><?php the_content(); ?></p>
						</div>
						
					<?php endwhile; ?>
					</div>
			<?php endif; ?>
	</div>
</div>


<?php get_footer(); ?>