<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="device=device-width, initial-scale=1.0">
	<title>Siete Sopas</title>
	<!--
		<link rel="stylesheet" href="css/estilos.css">
		<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
	-->
	<?php wp_head(); ?>
</head>
<body>
	
		<header class="header">
			<div class="container">
				<div class="header_arriba">
					<p>Delivery: 613 - 5000</p>
				</div>
			</div>
			<div class="header_medio">
				<div class="container">
					<h1 class="header_title">
						<a href="index.php">
							<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Siete Sopas">
						</a>
					</h1>
					<p>PROVOCA POR TODOS LADOS</p>
					<ul class="sociales_arriba">
						<li><a href="#"><span class="icon-facebook"></span></a></li>
						<li><a href="#"><span class="icon-instagram"></span></a></li>
					</ul>
				</div>
			</div>
			<button class="header_burger">
				Menú
			</button>
			<div class="container">
				<nav class="header_nav">
					<?php wp_nav_menu(array('theme_location'=>'primary','container'=>'ul','menu_class'=>'menu')); ?>
				</nav>
			</div>
			
		</header>
	
	