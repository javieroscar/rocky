<?php
	
function vinculando_estilos_y_js(){
	wp_enqueue_style('googlefonts','https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap');
	wp_enqueue_style('fancyboxcss', get_template_directory_uri().'/css/jquery.fancybox.min.css' );
	wp_enqueue_style('style', get_stylesheet_uri());
	wp_enqueue_script('cycle2', get_template_directory_uri().'/js/jquery.cycle2.min.js',array('jquery'),'1.0',true);
	wp_enqueue_script('app', get_template_directory_uri().'/js/app.js',array('jquery'),'1.0',true);
	wp_enqueue_script('fancybox', get_template_directory_uri().'/js/jquery.fancybox.min.js',array('jquery'),'1.0',true);
}

add_action('wp_enqueue_scripts', 'vinculando_estilos_y_js');


function registrar_menu(){
	register_nav_menu('primary','Menú Principal');
	register_nav_menu('secondary','Menú Secundario');
}

add_action('init','registrar_menu');

add_theme_support('post-thumbnails');


function registrar_sidebar(){
	register_sidebar( array(
		'name' => 'Barra Lateral de Widget',
		'id' => 'sidebar',
		'description' => 'Arrastrar los widgets aquí',
		'before_title' => '<h4 class="sidebar_title">',
		'after_title' => '</h4>'
	));
}

add_action('widgets_init', 'registrar_sidebar');


require_once(get_template_directory()."/inc/functions-filters.php");


