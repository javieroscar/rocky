<?php get_header(); ?>
<div class="outside">
	<div class="center controles-slider">
        <span id=prev><div class="icon-ctrl"></div></span>
        <span id=next><div class="icon-ctrl"></div></span>
    </div>
    <div class="slider">
		<style>
	#progress { position: absolute; bottom: 0; height: 6px; width: 0px; background: #c00; z-index: 500; }
	</style>
		<?php $queryposts1 = new WP_Query( array('category_name' => 'banner') ); if( $queryposts1->have_posts() ): ?>
					<div class="banner cycle-slideshow" data-cycle-fx="fadeout" data-cycle-prev="#prev" data-cycle-next="#next"data-cycle-slides="> div">
						
					<?php while( $queryposts1->have_posts() ): $queryposts1->the_post(); ?>
						<div class="banner-info">
							<?php the_post_thumbnail('post-thumbnail', array('class' => 'banner_image' ) ); ?>
							<a href="#">+Info</a>
						</div>
						
					<?php endwhile; ?>
					</div>
			<?php endif; ?>
	</div><!--fin slider-->
	<script>
		var progress = $('#progress'),
		    slideshow = $( '.cycle-slideshow' );

		slideshow.on( 'cycle-initialized cycle-before', function( e, opts ) {
		    progress.stop(true).css( 'width', 0 );
		});

		slideshow.on( 'cycle-initialized cycle-after', function( e, opts ) {
		    if ( ! slideshow.is('.cycle-paused') )
		        progress.animate({ width: '100%' }, opts.timeout, 'linear' );
		});

		slideshow.on( 'cycle-paused', function( e, opts ) {
		   progress.stop(); 
		});

		slideshow.on( 'cycle-resumed', function( e, opts, timeoutRemaining ) {
		    progress.animate({ width: '100%' }, timeoutRemaining, 'linear' );
		});
	</script>
</div>

<div class="container">

		<main class="main main--full">
				<section>
					<!-- h2 class="title">Nuestras Zapatillas</h2> -->
					<?php $querypost2 = new WP_Query(array('category_name' => 'zapatillas', 'posts_per_page' => 3 ) ) ?>
					<?php if($querypost2->have_posts() ): ?>
						<ul class="souplist">
							<?php while($querypost2->have_posts() ): $querypost2->the_post(); ?>
								<li class="souplist_item">
									<article class="soup">
										<div class="soup_img">
											<?php the_post_thumbnail('post-thumbnail', array('class' => 'soup_photo') ); ?>
										</div>
										<div class="soup_text">
											<h2 class="soup_title"><?php the_title(); ?></h2>
											<?php the_excerpt(); ?>
											<a href="<?php the_permalink(); ?>" class="soup_link">Ver receta</a>
										</div>
									</article>
								</li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					
				</section>
				<div class="container">
					<div class="frontpage_pie">
						<?php $queryposts1 = new WP_Query( array('category_name' => 'frontpage') ); if( $queryposts1->have_posts() ): ?>
								<div class="frontpage_all">
									
								<?php while( $queryposts1->have_posts() ): $queryposts1->the_post(); ?>
									<a href="#" class="frontpage">
										<?php the_post_thumbnail('post-thumbnail', array('class' => 'frontpage_image' ) ); ?>
										<div class="frontpage_content">
											<h3><?php the_title(); ?></h3>
											<p><?php the_content(); ?></p>
										</div>
										
									</a>
									
								<?php endwhile; ?>
								</div>
						<?php endif; ?>
					</div><!--fin slider-->
				</div>
				
		</main>
</div>

<?php get_footer(); ?>